﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace StudentExperienceApp
{
    public partial class Newspage : ContentPage
    {
        public Newspage()
        {
            InitializeComponent();
        }
        async void BackHome(object sender, System.EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}
