﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace StudentExperienceApp
{
    public partial class Timetable : ContentPage
    {
        public Timetable()
        {
            InitializeComponent();

            DisplayAlert("Attention", "Press on a cell in the timetable to enter class information", "OK");
        }

        
        async void BackHome(object sender, System.EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}
