﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace StudentExperienceApp
{
    public partial class WebView : ContentPage
    {
        public WebView(string Url)
        {
            InitializeComponent();
            Browser.Source = Url;
        }
        private void backClicked(object sender, EventArgs e)
        {

            if (Browser.CanGoBack)
            {
                Browser.GoBack();
            }
            else {
                Navigation.PopAsync();
            }
        }

        private void forwardClicked(object sender, EventArgs e)
        {
            if (Browser.CanGoForward)
            {
                Browser.GoForward();
            }
        }
    }
}
