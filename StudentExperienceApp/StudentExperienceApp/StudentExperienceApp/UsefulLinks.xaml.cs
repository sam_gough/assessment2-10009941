﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace StudentExperienceApp
{
    public partial class UsefulLinks : ContentPage
    {

        public string Url { get; set; }
        public UsefulLinks()
        {
            InitializeComponent();
        }
        async void ToiohomaiClick(object sender, System.EventArgs e)
        {
            Url = "https://toiohomai.ac.nz/";
            await Navigation.PushAsync(new WebView(Url));
        }
        async void MoodleClick(object sender, System.EventArgs e)
        {
            Url = "https://adfs.boppoly.ac.nz/adfs/ls/?SAMLRequest=pVLBbuIwEP2VyPckOIRUWIBEi6oitV1U6B72shrioVhybNczabf79RtCq9IeuOzJ8pt5b56fZ0LQ2KDmLe%2FdAz63SJz8aawj1Remoo1OeSBDykGDpLhW6%2FndrSqygQrRs6%2B9FSeU8wwgwsjGO5EsF1Pxuyh243JcDkegt1Ul6zFIWeuLra5wKEelHCEiVGUxLkXyEyN1zKnohDo6UYtLRwyOO2ggq1QWqSw3g4EaDZUsf4lk0b3GOOCetWcOpPIc9I6yrQ%2FB27cM6sz97aHcUi6S%2BYe%2FK%2B%2BobTCuMb6YGh8fbj8VGu%2B1xeKbCJkmWDxE0DXo1mIW9iHv73Q8ixRq6lGNO2gtpxREsnoP8dI4bdzT%2Bfy2xyZSN5vNKl39WG%2FEbHLQVn0ecfY%2FHhtk0MDwzeIkPx0wOS7MfWdtuVh5a%2Bq35NrHBvi88wNidLrrW1U4%2FCUxOu4yt9a%2FXkUExqng2KLIZ8eZX%2Fdy9g8%3D&RelayState=https%3A%2F%2Fmoodle2.boppoly.ac.nz%2Fauth%2Fsaml%2F&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=mMcFkRnkcEyDcsyXmRAclbiP26HECN546pa%2FfssRAmOPGx7AnPVMURVYDSduEUetP%2FkW8wfLv%2FJOWsJuUscv9zl7G4awthgKBwLJlqD5REz9GK773waiMK%2FcYvMAnVGtOaykfRgYQNzgz%2BC1mRj5s1ooXmxWi5opO1WNZHMLpnQY1f6h3U8Ja7BvhzvV0Me4UM3UGbQC2XinMYgEhaDV52QgnVOCyq20ZgBIqurvjA%2Bz%2FzLNUBrSAe2Xg9Qpo10pnu%2BZ9gkSicsOYOS59LhI33d9dCmIUtlnUmbOv4SO2HM7YaRLOota5IiZ4%2B9I%2B4anjPbQCYn9D4jMyt1pHOgzFA%3D%3D";
            await Navigation.PushAsync(new WebView(Url));
        }
        async void SlackClick(object sender, System.EventArgs e)
        {
            Url = "https://wbopp-it.slack.com";
            await Navigation.PushAsync(new WebView(Url));
        }
        async void FacebookClick(object sender, System.EventArgs e)
        {
            Url = "https://www.facebook.com/toiohomai/";
            await Navigation.PushAsync(new WebView(Url));
        }

        async void BackHome(object sender, System.EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}
