﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace StudentExperienceApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        async void PushNewsPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Newspage());
        }
        async void PushTimetablePage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Timetable());
        }
        async void PushUsefullinksPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new UsefulLinks());
        }
    }
}
